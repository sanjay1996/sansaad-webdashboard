import React, { Component } from "react";
import Cookies from 'js-cookie';
import { Redirect } from "react-router-dom";

export default ChildComponent => {
    class RequireAuth extends Component {
        render() {
            switch (Cookies.get("token")) {
                case undefined:
                    return <Redirect to="/login" />;
                case null:
                    return <Redirect to="/login" />;
                default:
                    return <ChildComponent {...this.props} />;
            }
        }
    }

    return RequireAuth;
};