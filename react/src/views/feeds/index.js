import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, FormGroup, Label, Input, Button } from "reactstrap";

import requireAuth from "../../components/hoc";

import {
  fetchSocialFeedLinks,
  postSocialFeedLinks
} from "../../redux/socialFeeds/action";

class SocialFeeds extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instagram: "",
      youtube: "",
      twitter: "",
      consumer_key: "",
      consumer_secret: "",
      access_token_key: "",
      access_token_secret: "",
      facebook: ""
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  componentDidMount = () => {
    this.props.fetchSocialFeedLinks();
  };

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.socialFeeds != null &&
      nextProps.socialFeeds != this.props.socialFeeds
    ) {
      let { instagram, youtube, facebook, access_token_key, consumer_key, consumer_secret, access_token_secret } = nextProps.socialFeeds;
      this.setState({ instagram, youtube, facebook, access_token_key, consumer_key, consumer_secret, access_token_secret });
    }
  }

  onChangeHandler(e) {
    let state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  }
  onSubmit() {
    this.props.postSocialFeedLinks(this.state);
  }

  render() {
    let { instagram, youtube, twitter, facebook } = this.state;
    return (
      <Row>
        <Col sm="12">
          <div className="card-box">
            <Row>
              <Col sm="12">
                <FormGroup>
                  <Label for="contact_longitude">Twitter</Label><br />
                  <Label for="contact_longitude">Twitter consumer key</Label>
                  <Input
                    type="text"
                    name="consumer_key"
                    value={this.state.consumer_key}
                    onChange={e => this.setState({ consumer_key: e.target.value })}
                  />
                  <Label for="contact_longitude">Twitter consumer secret</Label>
                  <Input
                    type="text"
                    name="consumer_secret"
                    value={this.state.consumer_secret}
                    onChange={e => this.setState({ consumer_secret: e.target.value })}
                  />
                  <Label for="contact_longitude">Twitter access token key</Label>
                  <Input
                    type="text"
                    name="access_token_key"
                    value={this.state.access_token_key}
                    onChange={e => this.setState({ access_token_key: e.target.value })}
                  />
                  <Label for="contact_longitude">Twitter access token secret</Label>
                  <Input
                    type="text"
                    name="access_token_secret"
                    value={this.state.access_token_secret}
                    onChange={e => this.setState({ access_token_secret: e.target.value })}
                  />
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup>
                  <Label for="contact_longitude">YouTube</Label>
                  <Input
                    type="text"
                    name="youtube"
                    value={youtube}
                    onChange={e => this.onChangeHandler(e)}
                  />
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup>
                  <Label for="contact_longitude">Instagram</Label>
                  <Input
                    type="text"
                    name="instagram"
                    value={instagram}
                    onChange={e => this.onChangeHandler(e)}
                  />
                </FormGroup>
              </Col>
              {/* <Col sm="12">
                <FormGroup>
                  <Label for="contact_longitude">Facebook</Label>
                  <Input
                    type="text"
                    name="facebook"
                    value={facebook}
                    onChange={e => this.onChangeHandler(e)}
                  />
                </FormGroup>
              </Col> */}
            </Row>
            <Row className="text-right">
              <Col sm="12">
                <Button
                  color="success"
                  className="text-uppercase"
                  onClick={() => this.onSubmit()}
                >
                  Save
                </Button>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchSocialFeedLinks: payload => {
      dispatch(fetchSocialFeedLinks(payload));
    },
    postSocialFeedLinks: payload => {
      dispatch(postSocialFeedLinks(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    socialFeeds: state.socialFeeds.socialFeeds
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(SocialFeeds));
