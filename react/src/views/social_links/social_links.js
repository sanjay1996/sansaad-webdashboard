import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { connect } from 'react-redux'
import { fetchSocialLinks, editSocialLinks } from '../../redux/social/actions';
import Cookies from 'js-cookie';

import requireAuth from '../../components/hoc';

class SocialLinks extends Component {

    constructor(props) {
        super(props)

        this.state = {
            _id: null,
            facebookLink: "",
            instagramLink: "",
            twitterLink: "",
            youtubeLink: "",
            userId: null
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //WARNING! To be deprecated in React v17. Use componentDidMount instead.
    componentWillMount() {
        this.props.onfetchSocialLinks()
        this.setState({
            userId: Cookies.get("userId")
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.socialLinks !== null) {
            nextProps.socialLinks.forEach(data => {
                this.setState({
                    facebookLink: data.facebookLink,
                    instagramLink: data.instagramLink,
                    twitterLink: data.instagramLink,
                    youtubeLink: data.youtubeLink,
                })
            })
        }
    }


    handleSubmit(e) {
        e.preventDefault();
        const data = { ...this.state }
        this.props.oneditSocialLinks(data)
    }
    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form onSubmit={this.handleSubmit}>
                            <Row>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="fb_link">Facebook Link</Label>
                                        <Input
                                            type="text"
                                            name="fb_link"
                                            id="fb_link"
                                            placeholder="Enter Your Facebook Page Link"
                                            value={this.state.facebookLink}
                                            onChange={(e) => this.setState({ ...this.state, facebookLink: e.target.value })}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="twitter_link">Twitter Link</Label>
                                        <Input
                                            type="text"
                                            name="twitter_link"
                                            id="twitter_link"
                                            placeholder="Enter Your Twitter Page Link"
                                            value={this.state.twitterLink}
                                            onChange={(e) => this.setState({ ...this.state, twitterLink: e.target.value })}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="youtube_link">Youtube Link</Label>
                                        <Input
                                            type="text"
                                            name="youtube_link"
                                            id="youtube_link"
                                            placeholder="Enter Your FacYoutubeebook Page Link"
                                            value={this.state.youtubeLink}
                                            onChange={(e) => this.setState({ ...this.state, youtubeLink: e.target.value })}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="instagram_link">Instagram Link</Label>
                                        <Input
                                            type="text"
                                            name="instagram_link"
                                            id="instagram_link"
                                            placeholder="Enter Your Instagram Page Link"
                                            value={this.state.instagramLink}
                                            onChange={(e) => this.setState({ ...this.state, instagramLink: e.target.value })}
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    <Button type="submit" color="success" className="text-uppercase">submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}


function mapDispatchToProps(dispatch) {
    return {
        onfetchSocialLinks: () => {
            dispatch(fetchSocialLinks());
        },
        oneditSocialLinks: (payload) => {
            dispatch(editSocialLinks(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        socialLinks: state.social.socialLinks
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(SocialLinks));
