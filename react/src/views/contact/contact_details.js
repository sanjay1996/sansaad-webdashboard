import React, { Component } from "react";
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap";
import { connect } from "react-redux";
import {
  saveContactDetails,
  onFetchContactDetails,
  fetchContactDetails
} from "../../redux/Contact/action";

import requireAuth from "../../components/hoc";
import Cookies from "js-cookie";
class ContactDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address: null,
      email: null,
      landlineNo: null,
      faxNo: null,
      latitude: null,
      longitude: null
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.contactDetail !== null) {
      nextProps.contactDetail.forEach(data => {
        this.setState({
          address: data.address,
          email: data.email,
          landlineNo: data.landlineNo,
          faxNo: data.faxNo,
          latitude: data.latitude,
          longitude: data.longitude
        });
      });
    }
  }

  componentWillMount() {
    this.props.onFetchContactDetails();
  }

  onSubmit() {
    const userData = {
      address: this.state.address,
      email: this.state.email,
      landlineNo: this.state.landlineNo,
      faxNo: this.state.faxNo,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      userId: Cookies.get("userId")
    };
    this.props.onSaveContactDetails(userData);
  }
  render() {
    if (this.props.contactDetail == null || undefined) {
      return <div>LOADING......</div>;
    } else {
      return (
        <Row>
          <Col sm="12">
            <div className="card-box">
              <Form>
                <Row>
                  <Col sm="12">
                    <FormGroup>
                      <Label for="address">Enter Your Address</Label>
                      <Input
                        type="textarea"
                        name="address"
                        id="address"
                        placeholder="Enter Your Address"
                        value={this.state.address}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            address: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="4">
                    <FormGroup>
                      <Label for="email">Enter Your Email</Label>
                      <Input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="Enter Your Email"
                        value={this.state.email}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            email: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="4">
                    <FormGroup>
                      <Label for="landline_no">Enter Landline No.</Label>
                      <Input
                        type="number"
                        name="landline_no"
                        id="landline_no"
                        placeholder="Enter Landline No."
                        value={this.state.landlineNo}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            landlineNo: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="4">
                    <FormGroup>
                      <Label for="fax_no">Enter Fax No.</Label>
                      <Input
                        type="number"
                        name="fax_no"
                        id="fax_no"
                        placeholder="Enter Fax No."
                        value={this.state.faxNo}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            faxNo: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="6">
                    <FormGroup>
                      <Label for="contact_latitude">
                        Enter Latitude For Google Map
                      </Label>
                      <Input
                        type="text"
                        name="contact_latitude"
                        id="contact_latitude"
                        placeholder="Enter Latitude For Google Map"
                        value={this.state.latitude}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            latitude: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col sm="6">
                    <FormGroup>
                      <Label for="contact_longitude">
                        Enter Longitude For Google Map
                      </Label>
                      <Input
                        type="text"
                        name="contact_longitude"
                        id="contact_longitude"
                        placeholder="Enter longitude For Google Map"
                        value={this.state.longitude}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            longitude: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row className="text-right">
                  <Col sm="12">
                    <Button
                      color="success"
                      className="text-uppercase"
                      onClick={() => this.onSubmit()}
                    >
                      submit
                    </Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </Col>
        </Row>
      );
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSaveContactDetails: payload => {
      dispatch(saveContactDetails(payload));
    },
    onFetchContactDetails: payload => {
      dispatch(onFetchContactDetails(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    image: state,
    contactDetail: state.contact.contactDetail
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(ContactDetails));
