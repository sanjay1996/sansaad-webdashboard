import React, { Component } from 'react'
import { Row, Col, FormGroup, Badge, Button } from 'reactstrap';
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO'
import { plus } from 'react-icons-kit/fa/plus'
import requireAuth from '../../components/hoc';
import { connect } from 'react-redux';
import { getImage1,getImage2,getImage3, saveFeaturedImage } from '../../redux/featured_image/action';

class Featured_image extends Component {
constructor(props) {
  super(props)

  this.state = {
     
  }
  this._handleImage1Change = this._handleImage1Change.bind(this)
  this._handleImage2Change = this._handleImage2Change.bind(this)
  this._handleImage3Change = this._handleImage3Change.bind(this)
  this.onSubmit = this.onSubmit.bind(this);
}

savePreview1(preview, Image) {
    const file = {
        preview, Image
    }
    this.props.onGetImage1(file)
}
savePreview2(preview, Image) {
    const file = {
        preview, Image
    }
    this.props.onGetImage2(file)
}
savePreview3(preview, Image) {
    const file = {
        preview, Image
    }
    this.props.onGetImage3(file)
}
    _handleImage1Change(e) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.savePreview1(reader.result, file);
                }
                reader.readAsDataURL(file)
            } else {
                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {

        }
    }
    _handleImage2Change(e) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.savePreview2(reader.result, file);
                }
                reader.readAsDataURL(file)
            } else {
                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {

        }
    }
     _handleImage3Change(e) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.savePreview3(reader.result, file);
                }
                reader.readAsDataURL(file)
            } else {
                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {

        }
    }
    onSubmit(){
        const data = {
            key: "featured image"
        }
        this.props.onSaveFeaturedImage(data)
    }

    render() {
        const index = 0;
        return (
            <div>
                <Row>
                    <Col sm="12">
                        <FormGroup>
                            <div class="panel1 panel-primary panelemail card-box">
                                <div class="panel-heading1">
                                    <Row>
                                        <Col sm="12">
                                            <span>Add Photos</span>
                                        </Col>
                                    </Row>
                                </div>
                                <div>
                                    {
                                        this.props.image1 === null ?
                                            <label for="image1" type="file" name="file" id="exampleFile" className="plus formcontrol">
                                                <Badge className="striped_border">
                                                    <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                </Badge>
                                            </label>
                                            :
                                            <div className="mr-3 mb-3 hvrbox" key={index} >
                                                <img src={this.props.image1.preview} style={{ height: '100px' }} />
                                                <div class="hvrbox-layer_top">
                                                    <div class="hvrbox-text">
                                                        <Icon icon={trashO} size={40} className="v-align-middle" style={{ color: '#fff' }} />
                                                    </div>
                                                </div>
                                            </div>
                                    }
                                    <input type="file" name="myfile" onChange={(e) => this._handleImage1Change(e)} id="image1" style={{ display: 'none' }} />
                                </div>
                                <div>
                                    {
                                        this.props.image2 === null ?


                                            <label for="image2" type="file" name="file" id="exampleFile" className="plus formcontrol">
                                                <Badge className="striped_border">
                                                    <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                </Badge>
                                            </label>



                                            :


                                            <div className="mr-3 mb-3 hvrbox" key={index} >
                                                <img src={this.props.image2.preview} style={{ height: '100px' }} />
                                                <div class="hvrbox-layer_top">
                                                    <div class="hvrbox-text">
                                                        <Icon icon={trashO} size={40} className="v-align-middle" style={{ color: '#fff' }} />
                                                    </div>
                                                </div>
                                            </div>

                                    }
                                    <input type="file" name="myfile" onChange={(e) => this._handleImage2Change(e)} id="image2" style={{ display: 'none' }} />
                                </div>
                                <div>
                                    {
                                        this.props.image3 === null ?


                                            <label for="image3" type="file" name="file" id="exampleFile" className="plus formcontrol">
                                                <Badge className="striped_border">
                                                    <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                </Badge>
                                            </label>



                                            :


                                            <div className="mr-3 mb-3 hvrbox" key={index} >
                                                <img src={this.props.image3.preview} style={{ height: '100px' }} />
                                                <div class="hvrbox-layer_top">
                                                    <div class="hvrbox-text">
                                                        <Icon icon={trashO} size={40} className="v-align-middle" style={{ color: '#fff' }} />
                                                    </div>
                                                </div>
                                            </div>

                                    }
                                    <input type="file" name="myfile" onChange={(e) => this._handleImage3Change(e)} id="image3" style={{ display: 'none' }} />
                                </div>
                                <Button color="success"
                                onClick={()=> this.onSubmit()}
                                className="text-uppercase" style={{ color: "#fff" }}>submit</Button>
                            </div>
                        </FormGroup>
                    </Col>
                </Row>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetImage1: (payload) => {
            dispatch(getImage1(payload));
        },
        onGetImage2: (payload) => {
            dispatch(getImage2(payload));
        },
        onGetImage3: (payload) => {
            dispatch(getImage3(payload));
        },
        onSaveFeaturedImage: (payload) => {
            dispatch(saveFeaturedImage(payload));
        },
    }
}

function mapStateToProps(state) {
    return {
        image1: state.featuredImage.image1,
        image2: state.featuredImage.image2,
        image3: state.featuredImage.image3
        }
}
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Featured_image));
