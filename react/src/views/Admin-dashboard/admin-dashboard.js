import React, { Component } from 'react'
import { Row } from 'reactstrap';
import CanvasJSReact from "./canvasjs.react";
import { connect } from 'react-redux';
import requireAuth from '../../components/hoc';
import { fetchAllAdmin, fetchAllUsers, fetchAllUsersMonthly } from "../../redux/superAdmin/action";

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Admin_dashboard extends Component {

    componentWillMount() {
        this.props.onFetchAllAdmin();
        this.props.onFetchAllUsers();
        this.props.onFetchAllUsersMonthly()
    }
    componentWillReceiveProps = (nextProps) => {
    }

    render() {
        const options = {
            animationEnabled: true,
            title: {
                // text: "Monthly Sales - 2017"
            },
            axisX: {
                valueFormatString: "MMM"
            },
            axisY: {
                // title: "Sales (in USD)",
                // prefix: "$",
                includeZero: false
            },
            data: [{
                // yValueFormatString: "$#,###",
                xValueFormatString: "MMMM",
                type: "spline",
                dataPoints: [
                    { x: new Date(2017, 0), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Jan },
                    { x: new Date(2017, 1), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Feb },
                    { x: new Date(2017, 2), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Mar },
                    { x: new Date(2017, 3), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Apr },
                    { x: new Date(2017, 4), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.May },
                    { x: new Date(2017, 5), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Jun },
                    { x: new Date(2017, 6), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Jul },
                    { x: new Date(2017, 7), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Aug },
                    { x: new Date(2017, 8), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Sept },
                    { x: new Date(2017, 9), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Oct },
                    { x: new Date(2017, 10), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Nov },
                    { x: new Date(2017, 11), y: this.props.monthlyUser == null ? 0 : this.props.monthlyUser.Dec }
                ]
            }]
        }
        return (
            <div>
                <Row>
                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div class="box bg-dager">
                            <div class="box-body">
                                <div class="text-center my-2" style={{ padding: '40px 0px' }}>
                                    <div class="font-size-60 text-white">{this.props.adminCount.length}</div>
                                </div>
                            </div>

                            <div class="box-body1 bg-gray-light py-12 text-center">
                                <span class="text-dark">Total Admin</span>
                            </div>

                            <div class="progress progress-xxs mt-0 mb-0">
                                <div class="progress-bar bg-dager" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div class="box bg-sucess">
                            <div class="box-body">
                                <div class="text-center my-2" style={{ padding: '40px 0px' }}>
                                    <div class="font-size-60 text-white">{this.props.userCount}</div>
                                </div>
                            </div>

                            <div class="box-body1 bg-gray-light py-12 text-center">
                                <span class="text-dark">Total Users</span>
                            </div>

                            <div class="progress progress-xxs mt-0 mb-0">
                                <div class="progress-bar bg-sucess" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <div className="card-box">
                            <h3 className="card-title">Admin</h3>
                            <div className="card-container">
                                <CanvasJSChart options={options} />
                            </div>
                        </div>
                    </div>

                </Row>

            </div>
        )
    }
}
function mapDispatchToProps(dispatch) {
    return {
        onFetchAllAdmin: () => {
            dispatch(fetchAllAdmin());
        },
        onFetchAllUsers: () => {
            dispatch(fetchAllUsers());
        },
        onFetchAllUsersMonthly: () => {
            dispatch(fetchAllUsersMonthly());
        }
    }
}

function mapStateToProps(state) {
    return {
        adminCount: state.superAdmin.adminFetch,
        userCount: state.superAdmin.usersFetch,
        monthlyUser: state.superAdmin.monthlyUsers
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Admin_dashboard));
