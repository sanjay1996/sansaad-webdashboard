import React, { Component } from 'react';
import { Row, Col, Table, Badge } from 'reactstrap';

import requireAuth from '../../components/hoc';

class Payments extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col sm="12">
                        <div className="card-box">
                            <Table striped hover bordered responsive>
                                <thead>
                                    <tr>
                                        <th>S. No</th>
                                        <th>Date</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th>Amount (Rs.)</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="warning" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="success" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="warning" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="success" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="warning" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>30/04/2018</td>
                                        <td>Naveen</td>
                                        <td>naveen@gmail.com</td>
                                        <td>8800710398</td>
                                        <td>500</td>
                                        <td><Badge color="success" className="text-capitalize badge-block">unsolved</Badge></td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default requireAuth(Payments)
