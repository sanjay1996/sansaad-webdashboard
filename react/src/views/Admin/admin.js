import React, { Component } from 'react';
import { Row, Col, Table, Badge, Form, Label, Input, Button, FormGroup } from 'reactstrap';
import { Icon } from 'react-icons-kit';
import { pencil } from 'react-icons-kit/fa/pencil'
import { trashO } from 'react-icons-kit/fa/trashO'
import { fetchAllAdmin, fetchAllUsers, fetchAllUsersMonthly, currentData, showForm } from "../../redux/superAdmin/action";
import { connect } from 'react-redux';
import requireAuth from '../../components/hoc';

class Admin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            table: true,
            details: false,
            form: false
        }
    }

    componentWillMount() {
        this.props.onFetchAllAdmin();
        // this.props.onFetchAllUsers();
        // this.props.onFetchAllUsersMonthly()
    }
    render() {
        let index = 0
        return (
            <div>
                <Row className="table">
                    <Col sm="12">

                        {this.props.showForm == true ?
                            <div className="card-box">
                                <Table striped hover bordered responsive>
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Users</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.props.admin.map((user, index) => (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td onClick={() => {
                                                        this.props.onCurrentData(user),
                                                            this.props.onShowForm(false)
                                                    }}>{user.name}</td>
                                                    <td>{user.phone}</td>
                                                    <td>{user.email}</td>
                                                    <td>700</td>
                                                    <td>
                                                        {/* <Badge onClick={() => this.setState({ table: false, details: false, form: true })} color="warning" className="badge-circle"><Icon icon={pencil} className="v-align-middle padd" style={{ color: '#fff' }} /></Badge> */}
                                                        <Badge color="danger" className="badge-circle"><Icon icon={trashO} className="v-align-middle padd" style={{ color: '#fff' }} /></Badge>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                            : <div className="card-box">
                                <Table hover bordered responsive>
                                    <Form className="col-md-12 col-lg-12 col-sm-12 form">

                                        <div className="col-md-12 col-lg-12 col-sm-12 formgroup form-group">
                                            <Label for="formInlineName" className="col-md-3 col-lg-3 col-sm-3  head-styling control-Label">
                                                <span>Name</span>
                                            </Label>
                                            <span>{this.props.currentData.name}</span>
                                        </div>

                                        <div className="col-md-12 col-lg-12 col-sm-12 formgroup form-group">
                                            <Label for="formInlineName" className="col-md-3 col-lg-3 col-sm-3  head-styling control-Label">
                                                <span>Mobile No.</span>
                                            </Label>
                                            <span>{this.props.currentData.phone}</span>
                                        </div>

                                        <div className="col-md-12 col-lg-12 col-sm-12 formgroup form-group">
                                            <Label for="formInlineName" className="col-md-3 col-lg-3 col-sm-3  head-styling control-Label">
                                                <span>Email</span>
                                            </Label>
                                            <span>{this.props.currentData.email}</span>
                                        </div>

                                        <div className="col-md-12 col-lg-12 col-sm-12 formgroup form-group">
                                            <Label for="formInlineName" className="col-md-3 col-lg-3 col-sm-3 head-styling control-Label">
                                                <span>Users</span>
                                            </Label>
                                            <span>700</span>
                                        </div>

                                        <div className="col-sm-12 text-right dim">
                                            <Button onClick={() => this.props.onShowForm(true)} type="button" color="danger" className="text-uppercase"
                                                style={{ color: "#fff" }}>
                                                close
                                        </Button>
                                        </div>
                                    </Form>
                                </Table>
                            </div>
                        }
                    </Col>
                </Row>
            </div>

        );
    }
}


function mapDispatchToProps(dispatch) {
    return {
        onFetchAllAdmin: () => {
            dispatch(fetchAllAdmin());
        },
        onFetchAllUsers: () => {
            dispatch(fetchAllUsers());
        },
        onFetchAllUsersMonthly: () => {
            dispatch(fetchAllUsersMonthly());
        },
        onCurrentData: (payload) => {
            dispatch(currentData(payload));
        },
        onShowForm: (payload) => {
            dispatch(showForm(payload));
        }
    }
}

function mapStateToProps(state) {
    return {
        currentData: state.superAdmin.currentData,
        showForm: state.superAdmin.showForm,
        admin: state.superAdmin.adminFetch,
        userCount: state.superAdmin.usersFetch,
        monthlyUser: state.superAdmin.monthlyUsers
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Admin));