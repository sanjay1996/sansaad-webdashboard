import React, { Component } from "react";
import { connect } from "react-redux";
import { Doughnut } from "react-chartjs-2";
import moment from "moment";
import {
  Row,
  Col,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";

import requireAuth from "../../../components/hoc";
import { fetchPollData, changePollStatus } from "../../../redux/poll/action";
import Cookies from "js-cookie";

const data = {
  labels: ["Option A", "Option B", "Option C", "Option D"],
  datasets: [
    {
      data: [300, 50, 100, 200],
      backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"],
      hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"]
    }
  ]
};

class ViewPolls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentItem: null,
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  componentDidMount = () => {
    this.props.fetchPollData();
  };

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.currentItem !== null &&
      nextProps.currentItem !== this.props.currentItem
    ) {
      const item = nextProps.currentItem;

      const data = {
        _id: item._id,
        question: item.question,
        status: item.status,
        labels: ["Option A", "Option B", "Option C", "Option D"],
        datasets: [
          {
            data: [
              item.responseA !== undefined ? item.responseA : 0,
              item.responseB !== undefined ? item.responseB : 0,
              item.responseC !== undefined ? item.responseC : 0,
              item.responseD !== undefined ? item.responseD : 0
            ],
            backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"]
          }
        ]
      };
      this.setState({
        ...this.state,
        currentItem: data
      });
    }
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  viewPoll(item) {
    const data = {
      _id: item._id,
      question: item.question,
      status: item.status,
      labels: ["Option A", "Option B", "Option C", "Option D"],
      datasets: [
        {
          data: [
            item.responseA !== undefined ? item.responseA : 0,
            item.responseB !== undefined ? item.responseB : 0,
            item.responseC !== undefined ? item.responseC : 0,
            item.responseD !== undefined ? item.responseD : 0
          ],
          backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#3f51b5"]
        }
      ]
    };
    this.setState({
      ...this.state,
      currentItem: data
    });
    this.toggle();
  }
  render() {
    return (
      <Row>
        <Col sm="12">
          <div className="card-box">
            <Table striped hover bordered responsive>
              <thead>
                <tr>
                  <td>S.No.</td>
                  <td>Question</td>
                  <td>Date</td>
                  <td>Status</td>
                </tr>
              </thead>
              <tbody>
                {this.props.pollsList.map((item, index) => (
                  <tr key={index} onClick={() => this.viewPoll(item)}>
                    <td>{index + 1}</td>
                    <td>{item.question}</td>
                    <td>{moment(item.date).format("DD MM YYYY")}</td>
                    <td> {item.status === true ? "Active" : "Inactive"}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Col>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle} />
          {this.state.currentItem !== null ? (
            <ModalBody>
              <p>
                <span className="font-500">Q.</span>{" "}
                {this.state.currentItem.question}
              </p>

              <Doughnut data={this.state.currentItem} />
            </ModalBody>
          ) : null}
          {this.state.currentItem !== null ? (
            <ModalFooter>
              {this.state.currentItem.status === false ? (
                <Button
                  color="primary"
                  onClick={() =>
                    this.props.changePollStatus({
                      userId : Cookies.get("userId"),
                      _id: this.state.currentItem._id,
                      status: true
                    })
                  }
                >
                  Activate
                </Button>
              ) : (
                  <Button
                    color="secondary"
                    onClick={() =>
                      this.props.changePollStatus({
                        userId : Cookies.get("userId"),
                        _id: this.state.currentItem._id,
                        status: false
                      })
                    }
                  >
                    Deactivate
                </Button>
                )}
            </ModalFooter>
          ) : null}
        </Modal>
        {/* <Col sm="6">
          <div className="card-box">
            <h3 className="card-title">Latest Poll Resuts</h3>
            <p>
              <span className="font-500">Q.</span> What is wrong with each of
              these methods
            </p>
            <div className="card-container">
              <Doughnut data={data} />
            </div>
          </div>
        </Col> */}
        {/* <Col sm="6">
          <div className="card-box">
            <h3 className="card-title">Latest Poll Resuts</h3>
            <p>
              <span className="font-500">Q.</span> What is wrong with each of
              these methods
            </p>
            <div className="card-container">
              <Doughnut data={data} />
            </div>
          </div>
        </Col> */}
        {/* <Col sm="6">
          <div className="card-box">
            <h3 className="card-title">Latest Poll Resuts</h3>
            <p>
              <span className="font-500">Q.</span> What is wrong with each of
              these methods
            </p>
            <div className="card-container">
              <Doughnut data={data} />
            </div>
          </div>
        </Col> */}
        {/* <Col sm="6">
          <div className="card-box">
            <h3 className="card-title">Latest Poll Resuts</h3>
            <p>
              <span className="font-500">Q.</span> What is wrong with each of
              these methods
            </p>
            <div className="card-container">
              <Doughnut data={data} />
            </div>
          </div>
        </Col> */}
        {/* <Col sm="6">
          <div className="card-box">
            <h3 className="card-title">Latest Poll Resuts</h3>
            <p>
              <span className="font-500">Q.</span> What is wrong with each of
              these methods
            </p>
            <div className="card-container">
              <Doughnut data={data} />
            </div>
          </div>
        </Col> */}
      </Row>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    fetchPollData: () => {
      dispatch(fetchPollData());
    },
    changePollStatus: payload => {
      dispatch(changePollStatus(payload));
    }
  };
}

function mapStateToProps(state) {
  return {
    pollsList: state.poll.pollList,
    currentItem: state.poll.currentItem
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(ViewPolls));
