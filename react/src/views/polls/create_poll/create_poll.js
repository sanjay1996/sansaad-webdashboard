import React, { Component } from "react";
import { Row, Col, Form, FormGroup, Input, Label, Button } from "reactstrap";
import { connect } from "react-redux";
import requireAuth from "../../../components/hoc";
import { pollData } from "../../../redux/poll/action";

class CreatePoll extends Component {
  constructor() {
    super();
    this.state = {
      question: "",
      name: "",
      shareholders: [
        { option: "" },
        { option: "" },
        { option: "" },
        { option: "" }
      ]
    };
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  handleOptionChange(index, event) {
    let newOptions = [...this.state.shareholders];
    newOptions[index].option = event.target.value;
    this.setState({
      ...this.state,
      shareholders: newOptions
    });
  }

  submit() {
    let status = true;
    this.state.shareholders.map((item, index) => {
      if (item.option == "") {
        status = false;
      }
    });

    if (status == true) {
      const pollData = {
        question: this.state.question,
        options: this.state.shareholders
      };
      this.props.onPollData(pollData);
    } else {
      alert("Please Fill all the Required Fields");
    }
  }

  render() {
    return (
      <Row>
        <Col sm="12">
          <div className="card-box">
            <div className="card-container">
              <Form>
                <Row>
                  <Col sm="12">
                    <FormGroup>
                      <Label for="poll_question">Enter Your Question</Label>
                      <Input
                        type="text"
                        name="poll_question"
                        id="poll_question"
                        placeholder="Enter Your Question"
                        value={this.state.question}
                        onChange={e =>
                          this.setState({
                            ...this.state,
                            question: e.target.value
                          })
                        }
                      />
                    </FormGroup>
                  </Col>
                  {this.state.shareholders.map((shareholder, idx) => (
                    <Col sm="6" key={idx}>
                      <FormGroup>
                        <Label for="option">{`Enter Option ${idx + 1}`}</Label>
                        <Input
                          type="text"
                          name="option"
                          id="option"
                          placeholder={`Enter Option ${idx + 1}`}
                          value={shareholder.option}
                          onChange={e => this.handleOptionChange(idx, e)}
                        />
                      </FormGroup>
                    </Col>
                  ))}
                </Row>
                <Row className="text-right">
                  <Col sm="12">
                    <Button
                      color="success"
                      className="text-uppercase"
                      onClick={() => this.submit()}
                    >
                      submit
                    </Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </div>
        </Col>
      </Row>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onPollData: payload => {
      dispatch(pollData(payload));
    }
  };
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(CreatePoll));
