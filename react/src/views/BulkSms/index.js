import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import requireAuth from '../../components/hoc';

 class BulkSms extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col sm="12">
                        <div className="card-box">
                            <iframe src="http://sms.pracharnama.com/" width="100%" height="500px" style={{border: "0px"}}></iframe>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default requireAuth(BulkSms)