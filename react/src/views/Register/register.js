import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from "axios";
import config from "../../config";
import { timingSafeEqual } from 'crypto';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: null,
            email: null,
            mobile: null,
            password: null,
            cPassword: null
        }
        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.state.password == this.state.cPassword) {
            const userData = {
                name: this.state.name,
                email: this.state.email,
                mobile: this.state.mobile,
                password: this.state.password,
    
            }
            axios.post(`${config.serverUrl}:${config.port}/node/api/admin/adminRegister`, userData).then((res) => {
                if (res.data.success === true) {
                    alert("admin registered");
                } else {
                    alert("admin not registered");
                }
            })
        } else {
            alert("Password didn't matched")
        }
        // this.props.onSaveContactDetails(userData);
    }

    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form onSubmit={this.onSubmit}> 
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="name">Name</Label>
                                        <Input type="text" name="name" id="name" placeholder="Enter Your name" required
                                        value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="email">Email</Label>
                                        <Input type="email" name="email" id="email" placeholder="Enter Your Email" required
                                        value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="mobile">Mobile</Label>
                                        <Input type="number" name="mobile" id="mobile" placeholder="Enter Mobile No." required
                                        value={this.state.mobile} onChange={(e) => this.setState({ mobile: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input type="password" name="password" id="password" placeholder="Enter your password" required
                                        value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="confirm_password">Confirm Password</Label>
                                        <Input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm your password" required
                                        value={this.state.cPassword} onChange={(e) => this.setState({ cPassword: e.target.value })} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    <Button type="submit" color="success" className="text-uppercase">submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }

}
export default Register;
