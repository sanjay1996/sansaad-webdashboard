import React, { Component } from 'react';
import { Row, Col, FormGroup, Label, Badge, Button } from 'reactstrap';

import requireAuth from '../../../components/hoc';


class ComplaintDetails extends Component {
    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Row>
                            <Col sm="4">
                                <FormGroup>
                                    <Label><strong>Complaint Date</strong></Label>
                                    <p>April 30, 2018</p>
                                </FormGroup>
                            </Col>
                            <Col sm="4">
                                <FormGroup className="text-right">
                                    <Label><strong>Status</strong></Label>
                                    <p><Badge color="success text-capitalize">solved</Badge></p>
                                </FormGroup>
                            </Col>
                            <Col sm="4">
                                <FormGroup>
                                    <Label><strong>Name</strong></Label>
                                    <p>Naveen Kumar Vedwal</p>
                                </FormGroup>
                            </Col>
                            <Col sm="4">
                                <FormGroup>
                                    <Label><strong>Email</strong></Label>
                                    <p>naveenvedwal@gmail.com</p>
                                </FormGroup>
                            </Col>
                            <Col sm="4">
                                <FormGroup>
                                    <Label><strong>Mobile No.</strong></Label>
                                    <p>Naveen Kumar Vedwal</p>
                                </FormGroup>
                            </Col>
                            <Col sm="12">
                                <FormGroup>
                                    <Label><strong>Message</strong></Label>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </FormGroup>
                            </Col>
                            <Col sm="12" className="text-right">
                                <Button type="button" color="success" className="text-capitalize">change status</Button>
                            </Col>

                        </Row>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default requireAuth(ComplaintDetails);
