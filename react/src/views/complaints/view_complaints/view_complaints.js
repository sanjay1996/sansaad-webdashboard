import React, { Component } from 'react';
import { Row, Col, Table, FormGroup, Label, Badge, Button } from 'reactstrap';
import { connect } from 'react-redux'
import moment from "moment";
import requireAuth from '../../../components/hoc';
import { fetchComplaints } from '../../../redux/complaints/action';


class ViewComplaints extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: false,
            item: null,
        }
    }

    //WARNING! To be deprecated in React v17. Use componentDidMount instead.
    componentWillMount() {
        this.props.onfetchComplaints()
    }

    render() {
        console.log('====================================');
        console.log(this.props.complaintsList);
        console.log('====================================');
        return (
            <Row>
                <Col sm="12">
                    {this.state.form == false ?
                        <div className="card-box">
                            <Table striped hover bordered responsive>
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th>Date</th>
                                        {/* <th>Status</th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.complaintsList.map((item, index) => (
                                        <tr key={index + 1}
                                            onClick={() => this.setState({
                                                form: true,
                                                item: item
                                            })}>
                                            <td>{index + 1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>{item.mobileNo}</td>
                                            <td>{moment(item.date).format('LL')}</td>
                                            {/* {item.status == true ?
                                                <td><Badge color="success" className="text-capitalize badge-block">Solved</Badge></td>
                                                : <td><Badge color="danger" className="text-capitalize badge-block">Unsolved</Badge></td>} */}
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </div> :

                        <div className="card-box">
                            <Row>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label><strong>Complaint Date</strong></Label>
                                        <p>{moment(this.state.item.date).format('LL')}</p>
                                    </FormGroup>
                                </Col>
                                {/* <Col sm="4">
                                    <FormGroup className="text-right">
                                        <Label><strong>Status</strong></Label>
                                        {this.state.item.status == true ?
                                                <p><Badge color="success text-capitalize">Solved</Badge></p>
                                                : <p><Badge color="danger text-capitalize">Unsolved</Badge></p>}
                                    </FormGroup>
                                </Col> */}
                                <Col sm="4">
                                    <FormGroup>
                                        <Label><strong>Name</strong></Label>
                                        <p>{this.state.item.name}</p>
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label><strong>Email</strong></Label>
                                        <p>{this.state.item.email}</p>
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label><strong>Mobile No.</strong></Label>
                                        <p>{this.state.item.mobileNo}</p>
                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label><strong>Message</strong></Label>
                                        <p>{this.state.item.complaint}</p>
                                    </FormGroup>
                                </Col>
                                <Col sm="12" className="text-right">
                                    <Button onClick={() => this.setState({
                                        form: false
                                    })} type="button" color="success" className="text-capitalize">Close</Button>
                                </Col>

                            </Row>
                        </div>
                    }
                </Col>
            </Row>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onfetchComplaints: (payload) => {
            dispatch(fetchComplaints(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        complaintsList: state.complaints.complaintsList
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(ViewComplaints));
