import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Badge
} from "reactstrap";
import { connect } from "react-redux";
import requireAuth from "../../components/hoc";
import {
  getImages,
  saveAchievementDetails,
  fetchAchievementData
} from "../../redux/Achievement/action";
import { Icon } from "react-icons-kit";
import { trashO } from "react-icons-kit/fa/trashO";
import { plus } from "react-icons-kit/fa/plus";

class Achievement extends Component {
  constructor(props) {
    super(props);

    this.state = {
      image: null,
      achievement: [{ title: "", description: "" }]
    };

    this.savePreview = this.savePreview.bind(this);
    this.handleAddAchievement = this.handleAddAchievement.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  savePreview(preview, Image) {
    const file = {
      preview,
      Image
    };
    this.props.onGetImages(file);
  }
  componentWillMount() {
    this.props.onFetchAchievementData();
  }
  componentWillReceiveProps = nextProps => {
    if (nextProps.data != undefined) {
      nextProps.data.forEach(data => {
        this.setState({
          image: data.image,
          achievement: data.achievement
        });
      });
    }
  };

  _handleImageChange(e) {
    e.preventDefault();
    var fileTypes = ["jpg", "jpeg", "png"];
    if (e.target.files[0]) {
      var extension = e.target.files[0].name
          .split(".")
          .pop()
          .toLowerCase(), //file extension from input file
        isSuccess = fileTypes.indexOf(extension) > -1;
      if (isSuccess) {
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
          this.savePreview(reader.result, file);
          this.setState({
            image: reader.result
          });
        };
        reader.readAsDataURL(file);
      } else {
        alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
      }
    } else {
    }
  }

  handleAddAchievement() {
    let state = this.state;
    state.achievement = [...state.achievement, { title: "", description: "" }];
    this.setState(state);
  }

  onChangeHandler(e, index) {
    e.preventDefault();
    let state = this.state;
    state.achievement[index][e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit() {
    const userData = {
      achievement: this.state.achievement,
      key: "achievementUpdate"
    };
    this.props.onSaveAchievementDetails(userData);
  }
  render() {
    return (
      <Row>
        <Col sm="12">
          <div className="card-box">
            <Row>
              <Col sm="12">
                <h3> Enter Your Achievement Details</h3>
              </Col>
            </Row>
            <Form>
              <Row>
                <FormGroup>
                  <Label for="rpofile_pic">Picture</Label>
                  {this.state.image === null ? (
                    <div>
                      <label
                        for="image123"
                        type="file"
                        name="file"
                        id="exampleFile"
                        className="plus formcontrol"
                      >
                        <Badge className="striped_border">
                          <Icon
                            icon={plus}
                            trashO
                            className="v-align-middle padded"
                            style={{ color: "#a6a6a6" }}
                          />
                        </Badge>
                      </label>
                      <input
                        type="file"
                        name="myfile"
                        onChange={e => this._handleImageChange(e)}
                        id="image123"
                        style={{ display: "none" }}
                      />
                    </div>
                  ) : (
                    <div className="mr-3 mb-3 hvrbox">
                      <img src={this.state.image} style={{ height: "100px" }} />
                      <div class="hvrbox-layer_top">
                        <div class="hvrbox-text">
                          <Icon
                            icon={trashO}
                            onClick={() => this.setState({ image: null })}
                            size={40}
                            className="v-align-middle"
                            style={{ color: "#fff" }}
                          />
                        </div>
                      </div>
                    </div>
                  )}
                </FormGroup>
              </Row>
              {this.state.achievement.map((item, index) => {
                return (
                  <Row>
                    <Col lg="4" md="4" sm="12">
                      <FormGroup>
                        <Label for="journey_details">Title </Label>
                        <Input
                          type="text"
                          className="form-control"
                          value={item.title}
                          name="title"
                          onChange={e => this.onChangeHandler(e, index)}
                        />
                      </FormGroup>
                    </Col>
                    <Col lg="8" md="8" sm="12">
                      <FormGroup>
                        <Label for="journey_details">Description </Label>
                        <Input
                          type="textarea"
                          rows="6"
                          name="description"
                          id="journey_details"
                          placeholder="Enter Your Achievement Details"
                          value={item.description}
                          onChange={e => this.onChangeHandler(e, index)}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                );
              })}
              {/* <Row>
                                <Col sm="12">
                                            <FormGroup>
                                            <Label for="journey_details">Images</Label>
                                            <div className="plus">
                                                  <Badge className="striped_border">
                                                    <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                </Badge>
                                            </div>
                                            </FormGroup>
                                        </Col>
                            </Row> */}
              <Row className="text-center">
                <Col cm="12">
                  <Button
                    color="blue"
                    className="text-uppercase"
                    onClick={() => this.handleAddAchievement()}
                  >
                    Add More
                  </Button>
                </Col>
              </Row>
              <Row className="text-right">
                <Col sm="12">
                  <Button
                    color="success"
                    className="text-uppercase"
                    onClick={() => this.onSubmit()}
                  >
                    submit
                  </Button>
                </Col>
              </Row>
            </Form>
          </div>
        </Col>
      </Row>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onGetImages: payload => {
      dispatch(getImages(payload));
    },
    onSaveAchievementDetails: payload => {
      dispatch(saveAchievementDetails(payload));
    },
    onFetchAchievementData: () => {
      dispatch(fetchAchievementData());
    }
  };
}

function mapStateToProps(state) {
  return {
    image: state.achievement.image,
    data: state.achievement.achievementData
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(requireAuth(Achievement));
