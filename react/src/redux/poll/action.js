import Cookies from "js-cookie";

import {
  SAVEPOLLDATA,
  GET_POLL_DATA,
  GET_POLL_DATA_SUCCESS,
  GET_POLL_DATA_FAILED,
  CHANGE_POLL_STATUS,
  CHANGE_POLL_STATUS_SUCCESS,
  CHANGE_POLL_STATUS_FAILED
} from "./actionType";

function savePollData(payload) {
  return {
    type: SAVEPOLLDATA,
    payload
  };
}

function fetchingPolldata() {
  return {
    type: GET_POLL_DATA
  };
}

function fetchingPolldataSuccess(payload) {
  return {
    type: GET_POLL_DATA_SUCCESS,
    payload
  };
}

function fetchingPolldataFailed(payload) {
  return {
    type: GET_POLL_DATA_FAILED,
    payload
  };
}

function changingPollStatus() {
  return {
    type: CHANGE_POLL_STATUS
  };
}

function changingPollStatusSuccess(payload) {
  return {
    type: CHANGE_POLL_STATUS_SUCCESS,
    payload
  };
}

function changingPollStatusFailed(payload) {
  return {
    type: CHANGE_POLL_STATUS_FAILED,
    payload
  };
}

export const pollData = pollData => {
  return (dispatch, getState, api) => {
    const userId = Cookies.get("userId");
    let data = {
      userId,
      ...pollData
    };
    api
      .post(`/admin/poll`, data)
      .then(res => {
        if (res.data.success === true) {
          dispatch(savePollData(res.data));
          alert("New poll saved Successfully");
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const fetchPollData = () => {
  return (dispatch, getState, api) => {
    dispatch(fetchingPolldata());
    const userId = Cookies.get("userId");
    api
      .get(`/users/getpoll`, { headers: { userId } })
      .then(res => {
        if (res.data.success === true) {
          dispatch(fetchingPolldataSuccess(res.data.data));
        } else {
          dispatch(fetchingPolldataFailed("Unable to Fetch Poll Data"));
        }
      })
      .catch(err => {
        dispatch(fetchingPolldataFailed("Unable to Fetch Poll Data"));
        console.log(err);
      });
  };
};

export const changePollStatus = data => {
  return (dispatch, getState, api) => {
    dispatch(changingPollStatus());

    api
      .post(`/admin/pollStatus`, data)
      .then(res => {
        if (res.data.success === true) {
          dispatch(changingPollStatusSuccess(res.data.data));
          dispatch(fetchPollData());
        } else {
          dispatch(changingPollStatusFailed("Unable To Change Status"));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(changingPollStatusFailed("Unable To Change Status"));
      });
  };
};
