import {
  SAVEPOLLDATA,
  GET_POLL_DATA_SUCCESS,
  CHANGE_POLL_STATUS,
  CHANGE_POLL_STATUS_SUCCESS
} from "./actionType";

const initialState = {
  pollData: null,
  pollList: [],
  currentItem: null
};

export default function PollReducer(state = initialState, action) {
  switch (action.type) {
    case SAVEPOLLDATA:
      return {
        ...state,
        profileData: action.payload.data
      };
    case GET_POLL_DATA_SUCCESS:
      return {
        ...state,
        pollList: action.payload
      };
    case CHANGE_POLL_STATUS:
      return {
        ...state,
        currentItem: null
      };
    case CHANGE_POLL_STATUS_SUCCESS:
      return {
        ...state,
        currentItem: action.payload
      };
    default:
      return state;
  }
}
