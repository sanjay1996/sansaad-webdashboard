export const SAVEPOLLDATA = "sansaad/poll/SAVEPOLLDATA";

export const GET_POLL_DATA = "sansaad/poll/GET_POLL_DATA";
export const GET_POLL_DATA_SUCCESS = "sansaad/poll/GET_POLL_DATA_SUCCESS";
export const GET_POLL_DATA_FAILED = "sansaad/poll/GET_POLL_DATA_FAILED";

export const CHANGE_POLL_STATUS = "sansaad/poll/CHANGE_POLL_STATUS";
export const CHANGE_POLL_STATUS_SUCCESS =
  "sansaad/poll/CHANGE_POLL_STATUS_SUCCESS";
export const CHANGE_POLL_STATUS_FAILED =
  "sansaad/poll/CHANGE_POLL_STATUS_FAILED";
