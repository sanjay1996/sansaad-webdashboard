import {
    FETCH_SOCIAL_LINKS,
    FETCH_SOCIAL_LINKS_SUCCESS,
    FETCH_SOCIAL_LINKS_FAILED,
} from './actionType';
import Cookies from 'js-cookie';

function fetchingSocialLinks() {
    return {
        type: FETCH_SOCIAL_LINKS,
    }
}

function fetchingSocialLinksSuccess(payload) {
    return {
        type: FETCH_SOCIAL_LINKS_SUCCESS,
        payload
    }
}

function fetchingSocialLinksFailed(payload) {
    return {
        type: FETCH_SOCIAL_LINKS_FAILED,
        payload
    }
}

export const fetchSocialLinks = () => (dispatch, getstate, api) => {
    const userId = Cookies.get("userId");
    api.get(`/users/getsocialLink`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        if (res.data.success === true) {
            dispatch(fetchingSocialLinksSuccess(res.data.data))
        }
    })
}

export const editSocialLinks = data => (dispatch, getstate, api) => {
    api.post(`/admin/socialLink`, data).then((res) => {
        alert(res.data.message)
        if (res.data.success === true) {
            dispatch(fetchSocialLinks())
        }
    })
}
