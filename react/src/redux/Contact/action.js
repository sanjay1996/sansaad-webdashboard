import { SAVECONTACTDATA,  FETCHCONTACTDETAILS } from './actionType';
import Cookies from 'js-cookie';

export function saveContactData(payload) {
    return {
        type: SAVECONTACTDATA,
        payload
    }
}


export function fetchContactDetails(payload) {
    return {
        type: FETCHCONTACTDETAILS,
        payload
    }
}

export const saveContactDetails = data => (dispatch, getState, api) => {
    const userId = Cookies.get("userId");
    let form = new FormData;
    form.append('address', data.address)
    form.append('email', data.email)
    form.append('landlineNo', data.landlineNo)
    form.append('faxNo', data.faxNo)
    form.append('latitude', data.latitude)
    form.append('longitude', data.longitude)
    form.append('userId', userId)

    api.post(`/admin/contact`, data).then((res) => {
        if (res.data.success === true) {
            dispatch(saveContactData(res.data.success))
            alert('Contact saved')
        }
        else {
            dispatch(saveContactData(res.data.success))
        }
    }).then(()=>{
        dispatch(onFetchContactDetails())
    }).catch((err) => {
        console.log(err);
    });
}

export const onFetchContactDetails = data => (dispatch, getState, api) => {
    const userId = Cookies.get("userId");
    api.get(`/users/getContact`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
        console.log('===========res.data=========================');
        console.log(res.data);
        console.log('===========res.data=========================');
        if (res.data.success === true) {
            dispatch(fetchContactDetails(res.data.data))
        }
    }).catch((err) => {
        console.log(err);
    });
}

