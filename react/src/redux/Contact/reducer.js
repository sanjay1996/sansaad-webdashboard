import { SAVECONTACTDATA, FETCHCONTACTDETAILS } from './actionType'

const initialState = {
    profileData: null,
    contactDetail: null
};

export default function ContactReducer(state = initialState, action) {
    switch (action.type) {
        case SAVECONTACTDATA:
            return {
                ...state,
                profileData: action.payload.data,
            };
        case FETCHCONTACTDETAILS:
            return {
                ...state,
                contactDetail: action.payload,
            };
        default:
            return state;
    }
}