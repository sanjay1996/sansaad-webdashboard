import {
    FETCH_COMPLAINTS_LIST,
    FETCH_COMPLAINTS_LIST_SUCCESS,
    FETCH_COMPLAINTS_LIST_FAILED,
} from "./actionType";

const initialState = {
    complaintsList: [],
}

export default function ComplaintsReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_COMPLAINTS_LIST_SUCCESS:
            return {
                ...state, complaintsList: action.payload
            }
        default:
            return state;
    }
}