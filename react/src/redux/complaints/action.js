import {
    FETCH_COMPLAINTS_LIST,
    FETCH_COMPLAINTS_LIST_SUCCESS,
    FETCH_COMPLAINTS_LIST_FAILED,
} from './actionType';
import Cookies from "js-cookie";
function fetchingComplaintsList() {
    return {
        type: FETCH_COMPLAINTS_LIST,
    }
}

function fetchingComplaintsListSuccess(payload) {
    return {
        type: FETCH_COMPLAINTS_LIST_SUCCESS,
        payload
    }
}

function fetchingComplaintsListFailed(payload) {
    return {
        type: FETCH_COMPLAINTS_LIST_FAILED,
        payload
    }
}

export const fetchComplaints = data => (dispatch, getstate, api) => {
    const userId= Cookies.get("userId");
    api.get(`/users/getComplaints`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingComplaintsListSuccess(res.data.data))
        }
    })
}