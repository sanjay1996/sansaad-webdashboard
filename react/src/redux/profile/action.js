import { SAVEPROFILEDATA, GET_IMAGE } from './actionType';
import Cookies from 'js-cookie';

export function saveProfileData(payload) {
    return {
        type: SAVEPROFILEDATA,
        payload
    }
}

export function getImages(payload) {
    return {
        type: GET_IMAGE,
        payload
    }
}

export const fetchProfileData = () => (dispatch, getState, api) => {
    const userId = Cookies.get("userId");
    api.get(`/users/getProfile`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } })
        .then((res) => {
            if (res.data.success === true) {
                dispatch(saveProfileData(res.data))
            }
        })
}

export const saveProfileDetails = data => (dispatch, getState, api) => {
    const userId = Cookies.get("userId");
    const Images = getState().profile.image;
    let form = new FormData;
    form.append('name', data.name)
    form.append('dateOfBirth', data.dob)
    form.append('about', data.about_yourself)
    form.append('qualification', data.qualification)
    // form.append('image', Images.Image)
    if (Images !== null) {
        form.append('image', Images.Image) 
    }
    form.append('userId', userId)

    api.post(`/admin/adminProfile`, form).then((res) => {
        if (res.data.success === true) {
            dispatch(saveProfileData(res.data.success))
            // dispatch(fetchProfileData())
            alert('profile saved')
        }
        else {
            dispatch(saveProfileData(res.data.success))
        }
    }).catch((err) => {
        console.log(err);
    });
}
