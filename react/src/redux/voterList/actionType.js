export const FETCH_VOTERS_LIST = 'sansaad/users/FETCH_VOTERS_LIST';
export const FETCH_VOTERS_LIST_SUCCESS = 'sansaad/users/FETCH_VOTERS_LIST_SUCCESS';
export const FETCH_VOTERS_LIST_FAILED = 'sansaad/users/FETCH_VOTERS_LIST_FAILED';