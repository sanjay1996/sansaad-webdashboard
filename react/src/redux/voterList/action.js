import {
    FETCH_VOTERS_LIST,
    FETCH_VOTERS_LIST_SUCCESS,
    FETCH_VOTERS_LIST_FAILED,
} from './actionType';
import Cookies from "js-cookie";
function fetchingVotersList() {
    return {
        type: FETCH_VOTERS_LIST,
    }
}

function fetchingVotersListSuccess(payload) {
    return {
        type: FETCH_VOTERS_LIST_SUCCESS,
        payload
    }
}

function fetchingVotersListFailed(payload) {
    return {
        type: FETCH_VOTERS_LIST_FAILED,
        payload
    }
}

export const fetchVoters = data => (dispatch, getstate, api) => {
    const userId= Cookies.get("userId");
    api.get(`/users/getVoters`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingVotersListSuccess(res.data.data))
        }
    })
}