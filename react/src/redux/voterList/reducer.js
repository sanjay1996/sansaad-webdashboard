import {
    FETCH_VOTERS_LIST,
    FETCH_VOTERS_LIST_SUCCESS,
    FETCH_VOTERS_LIST_FAILED,
} from "./actionType";

const initialState = {
    votersList: [],
}

export default function VotersReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_VOTERS_LIST_SUCCESS:
            return {
                ...state, votersList: action.payload
            }
        default:
            return state;
    }
}