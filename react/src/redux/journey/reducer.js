import { SAVEJOURNEDATA, GET_IMAGE } from './actionType'

const initialState = {
    journeyData: null,
    image: null
};

export default function JourneyReducer(state = initialState, action) {
    switch (action.type) {
        case SAVEJOURNEDATA:
        console.log('=============action.payload.data=======================');
        console.log(action.payload.data);
        console.log('=============action.payload.data=======================');
            return {
                ...state,
                journeyData: action.payload.data,
            };
        case GET_IMAGE:
            return {
                ...state,
                image: action.payload,
            };
        default:
            return state;
    }
}