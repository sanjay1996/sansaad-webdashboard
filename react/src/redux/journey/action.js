import { SAVEJOURNEDATA, GET_IMAGE } from './actionType';
import Cookies from 'js-cookie';


export function saveJourneyData(payload) {
    return {
        type: SAVEJOURNEDATA,
        payload
    }
}

export function getImages(payload) {
    return {
        type: GET_IMAGE,
        payload
    }
}

export const fetchJourneyData = () => (dispatch, getState, api) => {
    const userId= Cookies.get("userId");
    api.get(`/users/getJourney`, {headers : { 'Content-Type': 'application/x-www-form-urlencoded', userId }})
    .then((res) => {
        if (res.data.success === true) {
            dispatch(saveJourneyData(res.data))
        }
    })
}

export const saveJourneyDetails = data => (dispatch, getState, api) => {
    const userId= Cookies.get("userId");
    const Images = getState().journey.image;
    let form = new FormData;
    form.append('journey', data.journey)

    if(Images !== null){
        form.append('image', Images.Image)
    }
    
    form.append('userId', userId)
    
    api.post(`/admin/journey`, form).then((res) => {
        if (res.data.success === true) {
            dispatch(saveJourneyData(res.data.success))
            alert('journey saved')
        }
        else {
            dispatch(saveJourneyData(res.data.success))
        }
    }).catch((err) => {
        console.log(err);
    });
}
