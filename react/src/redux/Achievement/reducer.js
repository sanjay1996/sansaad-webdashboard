import { SAVEACHIEVEMENTDATA, GET_IMAGE } from './actionType'

const initialState = {
    achievementData: null,
    image: null
};

export default function AchievementReducer(state = initialState, action) {
    switch (action.type) {
        case SAVEACHIEVEMENTDATA:
        console.log('=============action.payload.data=======================');
        console.log(action.payload.data);
        console.log('=============action.payload.data=======================');
            return {
                ...state,
                achievementData: action.payload.data,
            };
        case GET_IMAGE:
            return {
                ...state,
                image: action.payload,
            };
        default:
            return state;
    }
}