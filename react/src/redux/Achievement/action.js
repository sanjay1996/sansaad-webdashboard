import { SAVEACHIEVEMENTDATA, GET_IMAGE } from "./actionType";
import Cookies from "js-cookie";

export function saveAchievementData(payload) {
  return {
    type: SAVEACHIEVEMENTDATA,
    payload
  };
}

export function getImages(payload) {
  return {
    type: GET_IMAGE,
    payload
  };
}

export const fetchAchievementData = () => (dispatch, getState, api) => {
  const userId = Cookies.get("userId");
  api
    .get(`/users/getAchievement`, {
      headers: { "Content-Type": "application/x-www-form-urlencoded", userId }
    })
    .then(res => {
      if (res.data.success === true) {
        dispatch(saveAchievementData(res.data));
      }
    });
};

export const saveAchievementDetails = data => (dispatch, getState, api) => {
  const userId = Cookies.get("userId");
  const Images = getState().achievement.image;
  const achievement = JSON.stringify(data.achievement);
  let form = new FormData();
  form.append("achievement", achievement);

  if (Images !== null) {
    form.append("image", Images.Image);
  }

  form.append("userId", userId);

  api
    .post(`/admin/achievement`, form)
    .then(res => {
      if (res.data.success === true) {
        dispatch(saveAchievementData(res.data.success));
        alert("achievement saved");
      } else {
        dispatch(saveAchievementData(res.data.success));
      }
    })
    .catch(err => {
      console.log(err);
    });
};
