export const FETCH_ALL_ADMINS_SUCCESS = "sansaad/superAdmin/FETCH_ALL_ADMINS_SUCCESS";
export const FETCH_ALL_ADMINS_FAIL = "sansaad/superAdmin/FETCH_ALL_ADMINS_FAIL";
export const FETCH_ALL_USERS_SUCCESS = "sansaad/superAdmin/FETCH_ALL_USERS_SUCCESS";
export const FETCH_ALL_USERS_FAIL = "sansaad/superAdmin/FETCH_ALL_USERS_FAIL";
export const FETCH_ALL_MONTHLY_USERS_SUCCESS = "sansaad/superAdmin/FETCH_ALL_MONTHLY_USERS_SUCCESS";
export const CURRENT_DATA = "sansaad/superAdmin/CURRENT_DATA";
export const SHOW_FORM = "sansaad/superAdmin/SHOW_FORM";