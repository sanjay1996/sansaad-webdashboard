import {
    FETCH_ALL_ADMINS_SUCCESS,
    FETCH_ALL_ADMINS_FAIL,
    FETCH_ALL_USERS_SUCCESS,
    FETCH_ALL_USERS_FAIL,
    FETCH_ALL_MONTHLY_USERS_SUCCESS,
    CURRENT_DATA,
    SHOW_FORM
} from './actionType'

const initialState = {
    adminFetch: [],
    monthlyUsers: null,
    usersFetch: null,
    currentData: null,
    showForm: true
    // usersFetchFail: null,
};

export default function superAdminReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_ALL_ADMINS_SUCCESS:
            return {
                ...state,
                adminFetch: action.payload,
            };
        case CURRENT_DATA:
            return {
                ...state,
                currentData: action.payload,
            };
        case SHOW_FORM:
            return {
                ...state,
                showForm: action.payload,
            };
        // case FETCH_ALL_ADMINS_FAIL:
        //     return {
        //         ...state,
        //         adminFetchFail: action.payload,
        //     };
        case FETCH_ALL_USERS_SUCCESS:
            return {
                ...state,
                usersFetch: action.payload,
            };
        // case FETCH_ALL_USERS_FAIL:
        //     return {
        //         ...state,
        //         usersFetchFail: action.payload,
        //     };
        case FETCH_ALL_MONTHLY_USERS_SUCCESS:
            return {
                ...state,
                monthlyUsers: action.payload,
            };
        default:
            return state;
    }
}