import {
    FETCH_ALL_ADMINS_SUCCESS,
    FETCH_ALL_ADMINS_FAIL,
    FETCH_ALL_USERS_SUCCESS,
    FETCH_ALL_USERS_FAIL,
    FETCH_ALL_MONTHLY_USERS_SUCCESS,
CURRENT_DATA,
SHOW_FORM
} from "./actionType";

export function fetchAllAdminSuccess(payload) {
    return {
        type: FETCH_ALL_ADMINS_SUCCESS,
        payload
    }
}

export function fetchAllAdminFail(payload) {
    return {
        type: FETCH_ALL_ADMINS_FAIL,
        payload
    }
}

export function currentData(payload) {
    return {
        type: CURRENT_DATA,
        payload
    }
}

export function showForm(payload) {
    return {
        type: SHOW_FORM,
        payload
    }
}

export function fetchAllUsersSuccess(payload) {
    return {
        type: FETCH_ALL_USERS_SUCCESS,
        payload
    }
}

export function fetchAllUsersFail(payload) {
    return {
        type: FETCH_ALL_USERS_FAIL,
        payload
    }
}

export function fetchAllMonthlyUsersSuccess(payload) {
    return {
        type: FETCH_ALL_MONTHLY_USERS_SUCCESS,
        payload
    }
}

export const fetchAllUsers = data => (dispatch, getState, api) => {
    api.get(`/superadmin/fetchAllUsers`).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchAllUsersSuccess(res.data.data.length))
        }
        else {
            dispatch(fetchAllUsersFail(res.data))
        }
    }).catch((err) => {
        console.log(err);
    });
}

export const fetchAllAdmin = data => (dispatch, getState, api) => {
    api.get(`/superadmin/adminRegister/fetchAdmin`).then((res) => {
        if (res.data.success === true) {
            console.log('====================================');
            console.log(res.data.data);
            console.log('====================================');
            dispatch(fetchAllAdminSuccess(res.data.data))
        }
        else {
            dispatch(fetchAllAdminFail(res.data))
        }
    }).catch((err) => {
        console.log(err);
    });
}

export const fetchAllUsersMonthly = data => (dispatch, getState, api) => {
    api.get(`/superadmin/fetchUserMonthlyCount`).then((res) => {
        console.log('============data========================');
        console.log(res.data.data);
        console.log('============data========================');
        if (res.data.success === true) {
            dispatch(fetchAllMonthlyUsersSuccess(res.data.data))
        }
    }).catch((err) => {
        console.log(err);
    });
}
