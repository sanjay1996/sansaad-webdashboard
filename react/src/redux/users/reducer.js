import {
    FETCH_USERS_LIST,
    FETCH_USERS_LIST_SUCCESS,
    FETCH_USERS_LIST_FAILED,
} from "./actionType";

const initialState = {
    usersList: [],
}

export default function UsersReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_LIST_SUCCESS:
            return {
                ...state, usersList: action.payload
            }
        default:
            return state;
    }
}