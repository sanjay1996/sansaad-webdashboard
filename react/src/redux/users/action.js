import {
    FETCH_USERS_LIST,
    FETCH_USERS_LIST_SUCCESS,
    FETCH_USERS_LIST_FAILED,
} from './actionType';
import Cookies from 'js-cookie';

function fetchingUsersList() {
    return {
        type: FETCH_USERS_LIST,
    }
}

function fetchingUsersListSuccess(payload) {
    return {
        type: FETCH_USERS_LIST_SUCCESS,
        payload
    }
}

function fetchingUsersListFailed(payload) {
    return {
        type: FETCH_USERS_LIST_FAILED,
        payload
    }
}

export const fetchUsers = data => (dispatch, getstate, api) => {
    const userId = Cookies.get("userId");
    api.get(`/users/fetchuser`, { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingUsersListSuccess(res.data.data))
        }
    })
}