export const GET_SOCIAL_FEEDS = "sansaad/socialFeeds/GET_SOCIAL_FEEDS";
export const GET_SOCIAL_FEEDS_SUCCESS =
  "sansaad/socialFeeds/GET_SOCIAL_FEEDS_SUCCESS";
export const GET_SOCIAL_FEEDS_FAILED =
  "sansaad/socialFeeds/GET_SOCIAL_FEEDS_FAILED";

export const POST_SOCIAL_FEEDS = "sansaad/socialFeeds/POST_SOCIAL_FEEDS";
export const POST_SOCIAL_FEEDS_SUCCESS =
  "sansaad/socialFeeds/POST_SOCIAL_FEEDS_SUCCESS";
export const POST_SOCIAL_FEEDS_FAILED =
  "sansaad/socialFeeds/POST_SOCIAL_FEEDS_FAILED";
