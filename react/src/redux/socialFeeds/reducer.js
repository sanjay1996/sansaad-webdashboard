import { GET_SOCIAL_FEEDS_SUCCESS } from "./actionType";

const initialState = {
  socialFeeds: null
};

export default function SocialFeedsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_SOCIAL_FEEDS_SUCCESS:
      return {
        ...state,
        socialFeeds: action.payload
      };
    default:
      return state;
  }
}
