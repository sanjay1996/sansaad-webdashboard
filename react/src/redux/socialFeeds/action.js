import Cookies from "js-cookie";

import {
  GET_SOCIAL_FEEDS,
  GET_SOCIAL_FEEDS_SUCCESS,
  GET_SOCIAL_FEEDS_FAILED,
  POST_SOCIAL_FEEDS,
  POST_SOCIAL_FEEDS_SUCCESS,
  POST_SOCIAL_FEEDS_FAILED
} from "./actionType";

function gettingSocialFeed() {
  return {
    type: GET_SOCIAL_FEEDS
  };
}
function gettingSocialFeedSuccess(payload) {
  return {
    type: GET_SOCIAL_FEEDS_SUCCESS,
    payload
  };
}
function gettingSocialFeedFailed(payload) {
  return {
    type: GET_SOCIAL_FEEDS_FAILED,
    payload
  };
}
function PostingSocialFeed() {
  return {
    type: POST_SOCIAL_FEEDS
  };
}
function PostingSocialFeedSuccess(payload) {
  return {
    type: POST_SOCIAL_FEEDS_SUCCESS,
    payload
  };
}
function PostingSocialFeedFailed(payload) {
  return {
    type: POST_SOCIAL_FEEDS_FAILED,
    payload
  };
}

export const fetchSocialFeedLinks = () => (dispatch, getstate, api) => {
  dispatch(gettingSocialFeed());
  const userId = Cookies.get("userId");
  api
    .get(`/users/getsocialFeedsLink`, {
      headers: { "Content-Type": "application/x-www-form-urlencoded", userId }
    })
    .then(res => {
      if (res.data.success === true) {
        dispatch(gettingSocialFeedSuccess(res.data.data));
      } else {
        dispatch(gettingSocialFeedFailed(res.data));
      }
    });
};

export const postSocialFeedLinks = data => (dispatch, getstate, api) => {
  dispatch(PostingSocialFeed());
  const userId = Cookies.get("userId");
  api.post(`/admin/socialFeedsLink`, { ...data, userId }).then(res => {
    if (res.data.success === true) {
      dispatch(PostingSocialFeedSuccess(res.data));
    } else {
      dispatch(PostingSocialFeedFailed(res.data));
    }
  });
};
