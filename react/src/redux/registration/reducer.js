import { REGISTRATION_SUCCESS } from "./actionType";

const initialState = {
    regitsrationSuccess: null,
}

export default function RegistrationReducer(state = initialState, action) {
    switch (action.type) {
        case REGISTRATION_SUCCESS:
            return {
                ...state, regitsrationSuccess: action.payload
            }
        default:
            return state;
    }
}