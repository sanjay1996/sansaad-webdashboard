import { REGISTRATION_SUCCESS } from "./actionType";

function registrationSuccess(payload){
    return{
        type: REGISTRATION_SUCCESS,
        payload
    }
}

export const registration = data => (dispatch, getstate, api)=> {
    api.post(`register`, data).then((res)=>{
        if(res.data.success === true){
            dispatch(registrationSuccess(res.data))
        }
    })
}