import {
  FETCH_SCHEDULES,
  FETCH_SCHEDULES_SUCCESS,
  FETCH_SCHEDULES_FAILED
} from "./actionType";
import Cookies from "js-cookie";

function fetchingSchedules() {
  return {
    type: FETCH_SCHEDULES
  };
}

function fetchingSchedulesSuccess(payload) {
  return {
    type: FETCH_SCHEDULES_SUCCESS,
    payload
  };
}

function fetchingSchedulesFailed(payload) {
  return {
    type: FETCH_SCHEDULES_FAILED,
    payload
  };
}

export const fetchSchedules = () => (dispatch, getstate, api) => {
  const userId = Cookies.get("userId");
  api
    .get(`/users/fetchMeeting`, {
      headers: { "Content-Type": "application/x-www-form-urlencoded", userId }
    })
    .then(res => {
      if (res.data.success === true) {
        dispatch(fetchingSchedulesSuccess(res.data.data));
      }
    });
};

export const addSchedule = data => (dispatch, getstate, api) => {
  api.post(`/admin/meeting`, data).then(res => {
    if (res.data.success === true) {
      alert("Schedule Added Successfully");
    }
  });
};
