import Cookies from 'js-cookie';
import { LOGIN_USER_SUCCESS, LOGIN_USER_FAILED, LOGIN_USER } from './actionType';

export function loginSuccess(payload) {
    return {
        type: LOGIN_USER_SUCCESS,
        payload
    }
}


export function loginFailed(payload) {
    return {
        type: LOGIN_USER_FAILED,
        payload
    }
}

export function loggingInUser(payload) {
    return {
        type: LOGIN_USER,
        payload
    }
}

export const loginUser = userCredentials => {
    return (dispatch, getState, api) =>
        api.post(`/auth/adminLogin`, userCredentials).then((res) => {
            if (res.data.success === true) {
                Cookies.set("token", res.data.jwtAccessToken);
                Cookies.set("userId", res.data.data._id);
                Cookies.set("userType", res.data.data.userType);
                console.log('===============cookioes=====================');
                console.log(Cookies.get('userType'));
                console.log('====================================');
                if (Cookies.get('userType') != undefined) {
                    dispatch(loginSuccess(res.data))
                }
                // console.log('====================================');
                // console.log(res.data);
                // console.log('====================================');
                // Cookies.set("token", res.data.jwtAccessToken);
                // Cookies.set("userId", res.data.data._id);
                // Cookies.set("userType", res.data.data.userType);
                // dispatch(loginSuccess(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};