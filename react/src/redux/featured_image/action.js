import { GET_IMAGE1, GET_IMAGE2, GET_IMAGE3 } from './actionType';
import Cookies from 'js-cookie';

export function getImage1(payload) {
    return {
        type: GET_IMAGE1,
        payload
    }
}

export function getImage2(payload) {
    return {
        type: GET_IMAGE2,
        payload
    }
}

export function getImage3(payload) {
    return {
        type: GET_IMAGE3,
        payload
    }
}

export const saveFeaturedImage = data => (dispatch, getState, api) => {
    const Image = [
        getState().featuredImage.image1.Image,
        getState().featuredImage.image2.Image,
        getState().featuredImage.image3.Image
    ]
    const userId = Cookies.get("userId");
    const userData = new FormData();
    userData.append('userId', userId)
    // userData.append('image', Image);
    Image.map((image, i) => {
        return userData.append('image', image)
    })
    api.post(`/admin/featuredImage`, userData).then((res) => {
        if (res.data.success === true) {
            alert('images uploaded')
        }
    })

}