import { GET_IMAGE1,GET_IMAGE2,GET_IMAGE3 } from './actionType';

const initialState = {
    image1: null,
    image2: null,
    image3: null,
}

export default function FeaturedImageReducer(state = initialState, action) {
    switch (action.type) {
        case GET_IMAGE1:
            return {
                ...state, image1: action.payload
            };
        case GET_IMAGE2:
            return {
                ...state, image2: action.payload
            };
        case GET_IMAGE3:
            return {
                ...state, image3: action.payload
            };
        default:
            return state;
    }
}