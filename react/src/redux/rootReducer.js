import { combineReducers } from "redux";
import register from "./registration/reducer";
import login from "./Auth/reducer";
import profile from "./profile/reducer";
import user from "./users/reducer";
import complaints from "./complaints/reducer";
import schedule from "./schedule/reducer";
import social from "./social/reducer";
import journey from "./journey/reducer";
import contact from "./Contact/reducer";
import featuredImage from "./featured_image/reducer";
import superAdmin from "./superAdmin/reducer";
import poll from "./poll/reducer";
import achievement from "./Achievement/reducer";
import socialFeeds from "./socialFeeds/reducer";
import voters from "./voterList/reducer";

export default combineReducers({
  register,
  login,
  profile,
  user,
  complaints,
  schedule,
  social,
  journey,
  contact,
  featuredImage,
  superAdmin,
  poll,
  achievement,
  socialFeeds,
  voters
});
